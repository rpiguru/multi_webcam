import glob
import threading
import http.client
import json
import sys
import os
import codecs
import cgi
import os
import time
import xml.etree.ElementTree
import datetime
from sparkpost import SparkPost

from logger import *

try:
    import BaseHTTPServer
except ImportError:
    import http.server as BaseHTTPServer
try:
    import urlparse
except ImportError:
    import urllib.parse as urlparse


class HttpHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    """HTTP handler that provides frontend for GPIO."""

    ENCODING = "UTF-8"
    server_version = "CameraWebServer/0.1"
    protocol_version = "HTTP/1.1"
    static_root = '/home/pi/multi_webcam/static'
    index_page = 'index.html'

    video_file = ''

    text_encoding_type = {
        'html': 'html',
        'htm': 'html',
        'js': 'js',
        'css': 'css'
    }
    binary_encoding_type = {
        'png': 'png',
        'jpg': 'jpg',
        'jpeg': 'jpg',
        'ttf': 'ttf',
        'woff': 'woff',
        'mp4': 'mp4',
        'gif': 'gif',
    }

    mime_type = {
        'text': 'text/plain',
        'html': 'text/html',
        'js': 'application/x-javascript',
        'css': 'text/css',
        'json': 'application/json',
        'png': 'image/png',
        'jpg': 'image/jpg',
        'gif': 'image/gif',
        'ttf': 'application/octet-stream',
        'woff': 'application/octet-stream',
        'mp4': 'application/octet-stream',
    }

    def _send_data(self, code, mime, data):
        assert type(data) == type(b"")
        self.send_response(code)
        self.send_header("Cache-Control", "no-cache")
        self.send_header("Content-Type", "{0}; charset={1}".format(mime, self.ENCODING))
        self.send_header("Content-Length", len(data))
        self.end_headers()
        self.wfile.write(data)

    def _send_raw_data(self, code, mime, data):
        assert type(data) == type(b"")
        self.send_response(code)
        self.send_header("Cache-Control", "no-cache")
        self.send_header("Content-Type", "{0}".format(mime))
        self.send_header("Content-Length", len(data))
        self.end_headers()
        self.wfile.write(data)

    def _send_error(self, code, message):
        data = message.encode(self.ENCODING)
        self._send_data(code, "text/html", data)
        # in this case connection will be close after 1 seconds

    def _send_message(self, message, mime):
        data = message.encode(self.ENCODING)
        self._send_data(http.client.OK, mime, data)

    def _send_binary_data(self, data, mime):
        self._send_data(http.client.OK, mime, data)

    @staticmethod
    def _get_json(data):
        """

        :param data:
        :return:
        """
        return json.dumps(data)

    # TODO: Use application-specific exception or ValueError
    def terminate_server(self):
        """

        :return:
        """
        logger.debug("in terminate_server")
        # terminate status/commands threads and exit with code
        data = self.server.gpio.get_status()
        text = self._get_json(data)
        logger.debug(text)
        sys.exit(0)

    def error(self, message):
        """

        :param message:
        :return:
        """
        self._send_error(http.client.NOT_FOUND, message)

    def no_page(self):
        """

        :return:
        """
        self.error(u"<p>NO SUCH PAGE</p>")

    def not_implemented(self):
        """

        :return:
        """
        self.error(u"<p>Not implemented yet</p>")

    def read_text_file(self, file_path, file_type):
        f = codecs.open(file_path, encoding='utf-8')
        if f is None:
            self._send_error(http.client.INTERNAL_SERVER_ERROR, "")
            return

        data = f.read()
        f.close()

        if file_type in self.mime_type:
            self._send_message(data, self.mime_type[file_type])
        else:
            self._send_error(http.client.INTERNAL_SERVER_ERROR, "")

    def read_binary_file(self, file_path, file_type):
        f = open(file_path, 'rb')
        if f is None:
            self._send_error(http.client.INTERNAL_SERVER_ERROR, "")
            return

        data = f.read()
        f.close()

        if file_type in self.mime_type:
            self._send_binary_data(data, self.mime_type[file_type])
        else:
            self._send_error(http.client.INTERNAL_SERVER_ERROR, "")

    def do_static(self):
        parsed = urlparse.urlparse(self.path)
        path = parsed.path
        if path == '/':
            path = self.index_page

        file_path = '{}/{}'.format(self.static_root, path)

        try:
            # check log file exists, if /log request already handled there are no log file in /tmp
            if not os.path.exists(file_path):
                self._send_error(http.client.NOT_FOUND, "")
                return

            ext = os.path.splitext(file_path)[1]
            if ext == '':
                self._send_error(http.client.NOT_FOUND, "")
                return

            file_type = ext[1:]
            if file_type in self.text_encoding_type:
                self.read_text_file(file_path, self.text_encoding_type[file_type])
            elif file_type in self.binary_encoding_type:
                self.read_binary_file(file_path, self.binary_encoding_type[file_type])
            else:
                self._send_error(http.client.NOT_IMPLEMENTED, "file type: {}".format(file_type))

        except IOError as e:
            logger.error("IOError: {0}".format(e))
            self.server.set_command_alive(False, self.request)
        except UnicodeDecodeError as e:
            self._send_error(http.client.INTERNAL_SERVER_ERROR, "{}".format(e))

    def do_echo(self):
        pass

    def do_GET(self):
        """

        :return:
        """
        logger.debug(threading.get_ident())

        try:
            GETMAP = {
                "/": self.do_static,
                "/echo": self.do_echo,
                "/take_picture": self.take_picture,
                "/send_mail": self.send_mail,
                "/get_video": self.get_video_file,
            }
            parsed = urlparse.urlparse(self.path)
            path = parsed.path
            handler = GETMAP.get(path, self.do_static)
            handler()

        except Exception as e:
            logger.error("Exception: {0}".format(e))

    def do_POST(self):
        """

        :return:
        """
        logger.debug(threading.get_ident())

        try:
            # Request data
            # content_length = self.headers.getheader('content-length')     python 2.x
            ctype, pdict = cgi.parse_header(self.headers.getheader('content-type'))
            if ctype == 'multipart/form-data':
                postvars = cgi.parse_multipart(self.rfile, pdict)
            elif ctype == 'application/x-www-form-urlencoded':
                length = int(self.headers.getheader('content-length'))
                postvars = urlparse.parse_qs(self.rfile.read(length), keep_blank_values=1)
            else:
                postvars = {}

            # content_len = self.headers.get_all('content-length')
            # size = int(''.join(content_len))
            # text = self.rfile.read(size)

            logger.debug(postvars)

            POSTMAP = {
                "/": self.do_static,
                "/echo": self.do_echo,
            }

        except Exception as e:
            # TODO: Use application-specific exception or ValueError
            logger.error("Exception: {0}".format(e))

    def take_picture(self):
        """
        Take 6 photos and create slide show video file
        """
        path = "/home/pi/multi_webcam/static/videos/"

        os.system("sudo rm " + path + "*.jpg")

        print("Old Files: ", glob.glob(path + "*.jpg"))

        for i in range(6):
            self.server.cam_list[i].take_pic()

        time.sleep(6)
        self.video_file = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        self.set_param_to_xml('VIDEO', self.video_file)

        # ffmpeg -framerate 5 -loop 1 -i %03d.jpg -c:v libx264 -r 30 -pix_fmt yuv420p -t 10 -y out.mp4

        cmd = "ffmpeg -an -framerate 10 -loop 1 -i " + path + "%03d.jpg -c:v libx264 -strict experimental -profile:v baseline -movflags faststart -pix_fmt yuv420p -f mp4 -preset slow -r 30 -t 10 -y " + \
              path + self.video_file + ".mp4"

        print('-------------------------- Creating video ------------------------')
        print("CMD: " + cmd)
        os.system(cmd)

        cmd = "ffmpeg -framerate 10 -f image2 -i " + path + "%03d.jpg -vf scale=500x500 -y " + path + \
              self.video_file + ".gif"
        print("CMD for GIF:" + cmd)
        os.system(cmd)
        self._send_message('true', "plain/text")

    def send_mail(self):
        """
        Send video file to the e-mail
        """
        parsed = urlparse.urlparse(self.path)
        email_address = parsed.query

        sp = SparkPost('5eeb6dad1c7018da1ecb2533f268b220cf45b7f7')

        content = '<p>Hey!</p><p>Thanks so much for visiting us.</p><p> Please find your video attached.</p>' \
                  ' <p>Much love.</p><p>Tens</p>'

        response = sp.transmissions.send(
            recipients=[email_address],
            html=content,
            from_email='gifs@tens.co',
            subject='Your video from Tens!',
            track_opens=True,
            attachments=[
                {
                    "name": "Tens.mp4",
                    "type": "video/mp4",
                    "filename": "/home/pi/multi_webcam/static/videos/" + self.get_param_from_xml("VIDEO") + ".mp4"
                }
            ]
        )

        print(response)
        if response['total_accepted_recipients'] == 1:
            self._send_message('true', 'plain/text')
        else:
            self._send_message('false', 'plain/text')

    def get_video_file(self):
        self._send_message(self.get_param_from_xml('VIDEO'), 'plain/text')

    def get_param_from_xml(self, param):
        """
        Get configuration parameters from the config.xml
        :param param: parameter name
        :return: if not exists, return None
        """
        conf_file_name = '/home/pi/multi_webcam/static/videos/config.xml'
        root = xml.etree.ElementTree.parse(conf_file_name).getroot()
        tmp = None
        for child_of_root in root:
            if child_of_root.tag == param:
                tmp = child_of_root.text
                break

        return tmp

    def set_param_to_xml(self, tag_name, new_val):
        conf_file_name = '/home/pi/multi_webcam/static/videos/config.xml'
        et = xml.etree.ElementTree.parse(conf_file_name)
        for child_of_root in et.getroot():
            if child_of_root.tag == tag_name:
                child_of_root.text = new_val
                et.write(conf_file_name)
                return True
        return False
