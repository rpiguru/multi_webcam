from sparkpost import SparkPost

sp = SparkPost('5eeb6dad1c7018da1ecb2533f268b220cf45b7f7')

response = sp.transmissions.send(
    recipients=['wester.de.weerdt@dutchmail.com'],
    html='<p>Hey!</p><p>Thanks so much for visiting us.</p><p>Please find your video attached.</p>'
         ' <p>Much love.</p><p>Tens</p>',
    from_email='gifs@tens.co',
    subject='Your video from Tens!',
    track_opens=True,
    attachments=[
        {
            "name": "result.gif",
            "type": "image/gif",
            "filename": "static/videos/out.gif"
        }
    ]
)

print(response)
