import os

for i in range(6):
    pipe = os.popen("udevadm info --attribute-walk /dev/video" + str(i) + " | grep serial")
    data = pipe.read().strip().split("\n")[0]
    pipe.close()
    print("Serial of /dev/video/" + str(i) + ":")
    print(data[16:-1])
